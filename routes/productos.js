var express = require('express');
var router = express.Router();
// Agregamos la variable debug que se desplegara 
// automaticamente en todos los registros de esta fuente.
// Debug depende de la variable NODE_ENV y para que clases estte definida
var debug = require('debug')('producto');

let Productos = require('../models/productos');

router.get('/', function (req, res, next) {
    Productos.find({}, function (err, productosRespuesta) {
        if (err) res.send(err);
        else {
            console.log(productosRespuesta);
            res.render('listado', {
                productos: productosRespuesta
            });
        }
    });
});

router.get('/registrarProductos', function (req, res, next) {
    res.render('registrar', {});
});

router.post('/guardarProductos', function (req, res, next) {
    console.log(req.body);
    var _id = req.body._id;
    var sku = req.body.sku;
    var nombre = req.body.nombre;
    var descripcion = req.body.descripcion;
    var precioUnitario = req.body.precioUnitario;

    if (_id !== '') {
        Productos.update({
                _id: _id
            }, {
                $set: {
                    sku: sku,
                    nombre: nombre,
                    descripcion: descripcion,
                    precioUnitario: precioUnitario
                }
            },
            function (err, resp) {
                if (err) {
                    // Agregamos debug para registrar el resultado 
                    // de acciones asíncronas y sincrónicas
                    debug('error - modificar:' + err);
                    res.send(err);
                } else {
                    // Agregamos debug para registrar el resultado 
                    // de acciones asíncronas y sincrónicas
                    debug('éxito - modificar:' + err);
                    res.redirect('/');
                }
            }
        );
    } else {
        let productosModelos = new Productos({
            sku: sku,
            nombre: nombre,
            descripcion: descripcion,
            precioUnitario: precioUnitario
        });
        productosModelos.save(function (err, resp) {
            if (err) {
                // Agregamos debug para registrar el resultado 
                // de acciones asíncronas y sincrónicas
                debug('error - guardar:' + err);
                res.send(err);
            } else {
                // Agregamos debug para registrar el resultado 
                // de acciones asíncronas y sincrónicas
                debug('éxito - guardar:' + err);
                res.redirect('/');
            }
        });
    }
});

router.get('/modificaProductos/:id', (req, res, next) => {
    let idProducto = req.params.id;
    Productos.findOne({
        _id: idProducto
    }, (err, producto) => {

        if (err) {
            // Agregamos debug para registrar el resultado 
            // de acciones asíncronas y sincrónicas
            debug('error - obtener:' + err);
            res.send(err);
        } else {
            // Agregamos debug para registrar el resultado 
            // de acciones asíncronas y sincrónicas
            debug('éxito - obtener:' + err);
            res.render('registrar', {
                producto: producto
            });
        }

    });
});

router.get('/eliminarProductos/:id', (req, res, next) => {
    let idProducto = req.params.id;

    Productos.remove({
        _id: idProducto
    }, err => {
        if (err) {
            // Agregamos debug para registrar el resultado 
            // de acciones asíncronas y sincrónicas
            debug('error - eliminar:' + err);
            res.send(err);
        } else {
            // Agregamos debug para registrar el resultado 
            // de acciones asíncronas y sincrónicas
            debug('éxito - eliminar:' + err);
            res.redirect('/');
        }
    });
});

module.exports = router;