var express = require('express');
var router = express.Router();

/* GET home page. */
// Redireccionamos el route raiz hacia el route /productos/
router.get('/', function (req, res, next) {
  res.redirect('/productos/');
});

module.exports = router;
