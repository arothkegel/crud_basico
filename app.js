var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
// Agregamos el archivo productos.js
var productosRouter = require('./routes/productos.js');

// Agregamos gzip/deflate para la compresion de las respuesta
// Esto debe aparecer antes que cualquier o todos los routes que deseamos comprimir
var compression = require('compression');

// Create the Express application object
var app = express();

// Comprimirá todas las repuesta de los routes
app.use(compression());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);
// Agregamos una nueva ruta de endpoint para el archivo productos.js
app.use('/productos', productosRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// Conexion Mongoose y MongoDB
mongoose.connect('mongodb://localhost:27017/crud_basico', {
  useNewUrlParser: true
});

// A esta
// importamos la configuracion desde un archivo de configuracion
// var configDB = require('./config/database.js');

// Configuracion de mongoose
// mongoose.connect(configDB.url, {
//   useMongoClient: true
// }); // COneccion a la base de datos

module.exports = app;