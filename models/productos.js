var mongoose = require('mongoose');
var Schema = mongoose.Schema;

let productosSchema = new Schema({
    sku: String,
    nombre: String,
    descripcion: String,
    precioUnitario: Number
});

module.exports = mongoose.model('Productos', productosSchema, 'productos');
